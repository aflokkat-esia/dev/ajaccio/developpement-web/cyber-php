<?php
$age = 20;
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TP2</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <?php
        $age = 15;
        if ($age < 18) {
            echo '<div class="alert warning"><p><span class="bold">Désolé ! </span>Vous ne pouvez pas accéder à ce site.</p></div>';
        } else {
            echo '
            <div class="alert success">
            <p><span class="bold">Bravo ! </span>Vous pouvez accéder au contenu.</p>
            </div>';
        }
    ?>
</body>

</html>