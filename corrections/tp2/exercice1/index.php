<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TP2</title>
</head>

<body>
    <?php
    $temperature = 36;

    if ($temperature > 30) {
        echo "Il fait chaud";
    } elseif ($temperature < 10) {
        echo "Il fait froid";
    } else {
        echo "Il fait bon";
    }

    ?>
</body>

</html>