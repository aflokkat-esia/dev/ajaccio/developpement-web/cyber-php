<?php
$commentaires = [
    [
        'auteur' => 'Jean',
        'contenu' => 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Deleniti, numquam mollitia! Earum numquam rerum molestiae aut dolore? Iusto assumenda qui possimus mollitia, sint ipsam iure, repellat eaque rem placeat dicta.',
        'avatar' => 'http://i9.photobucket.com/albums/a88/creaticode/avatar_1_zps8e1c80cd.jpg',
    ],
    [
        'auteur' => 'Thomas',
        'contenu' => 'Numquam mollitia! Earum numquam rerum molestiae aut dolore? Iusto assumenda qui possimus mollitia',
        'avatar' => 'http://i9.photobucket.com/albums/a88/creaticode/avatar_2_zps7de12f8b.jpg',
    ],
    [
        'auteur' => 'Fanny',
        'contenu' => 'Adipisicing elit !',
        'avatar' => 'http://i9.photobucket.com/albums/a88/creaticode/avatar_1_zps8e1c80cd.jpg',
    ]
];
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TP 13</title>
    <link rel="stylesheet" href="./style.css">
</head>

<body>
    <div class="comments-container">
        <h1>Commentaires</h1>

        <ul class="comments-list">

            <!-- START COMMENTAIRE -->
            <?php foreach($commentaires as $commentaire) { ?>
            <li>
                <img class='comment-avatar' src="<?php echo $commentaire['avatar']; ?>">

                <div class="comment-box">
                    <h2 class="comment-autor"><?php echo $commentaire['auteur']; ?></h2>
                    <p class="comment-content"><?php echo $commentaire['contenu']; ?></p>
                </div>
            </li>
            <?php } ?>

            <!-- FIN COMMENTAIRE -->

        </ul>
    </div>

</body>

</html>